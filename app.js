const express = require('express');
const app = express();
const cors = require("cors");
const multer = require('multer');
const {mongoose} = require('./db/mongoose');
const bodyParser = require('body-parser');
const { List } = require('./db/models/list.model');
const { Task } = require('./db/models/task.model');
const { Class } = require('./db/models/class.model');
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());



app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.setHeader("Access-Control-Max-Age", "1800");
		res.setHeader("Access-Control-Allow-Headers", "content-type");
		res.setHeader("Access-Control-Allow-Methods","PUT, POST, GET, DELETE, PATCH, OPTIONS");
		// res.setHeader("Content-Type", "application/json;charset=utf-8"); // Opening this comment will cause problems
    next();
  });


  const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'uploads')
    },
    filename: (req, file, callBack) => {
        callBack(null, `FunOfHeuristic_${file.originalname}`)
    }
  })
  
const upload = multer({ storage: storage });

 app.post('/file', upload.single('file'), (req, res, next) => {
    const file = req.file;
    let title = req.body.title;
    let image = file.filename;
    if (!file) {
      const error = new Error('No File')
      error.httpStatusCode = 400
      return next(error)
    }

    let newList = new List({
        title,image
    });
    newList.save().then((listDoc) =>{
        res.send(listDoc);
    });
      //res.send(file);
  })

app.get('/lists',(req, res) =>{
    //res.send("Hello World!!!");
    List.find({}).then((lists) =>{
        res.send(lists);
    })
})

// Start Class Operation   =========================================================================================

app.get('/class',(req, res) =>{
    //res.send("Hello World!!!");
    Class.find({}).then((lists) =>{
        res.send(lists);
    })
})
app.post('/class',(req,res) =>{

    let title = req.body.title;
    let newClass = new Class({
        title
    });
    newClass.save().then((listDoc) =>{
        res.send(listDoc);
    });
})

app.patch('/class/:id',(req,res) =>{
    //code goes here
    Class.findByIdAndUpdate({ _id: req.params.id},{
        $set: req.body
    }).then((lists) =>{
        res.send(lists);
        //res.sendStatus(200);
    });
});

app.delete('/class/:id',(req,res) =>{
    //code goes here
    Class.findByIdAndDelete({
        _id: req.params.id
    }).then((removedListDoc) =>{
        res.send(removedListDoc);
    })
})

///End Class Operation  ==============================================================================================

app.get('/lists/:listId/tasks',(req, res) =>{
    //res.send("Hello World!!!");
    Task.find({
        _listId:req.params.listId
    }).then((tasks) =>{
        res.send(tasks);
    })
})

app.get('/lists/:listId/tasks/:taskId',(req, res) =>{
    //res.send("Hello World!!!");
    Task.find({
        _id:req.params.taskId,
        _listId:req.params.listId
    }).then((task) =>{
        res.send(task);
    })
})

app.post('/lists',(req,res) =>{

    let title = req.body.title;
    let newList = new List({
        title
    });
    newList.save().then((listDoc) =>{
        res.send(listDoc);
    });
})

app.post('/lists/:listId/tasks',(req, res) =>{
    let newTask = new Task({
        title: req.body.title,
        _listId: req.params.listId
    });
    newTask.save().then((newTaskDoc) =>{
        res.send(newTaskDoc);
    });
})

app.patch('/lists/:listId/tasks/:taskId', (req, res) =>{
    Task.findByIdAndUpdate({
        _id:req.params.taskId,
        _listId: req.params.listId
    },{
        $set: req.body
    }
    ).then(() =>{
        res.sendStatus(200);
    })
});

app.delete('/lists/:listId/tasks/:taskId', (req, res) => {
    Task.findByIdAndRemove({
        _id: req.params.taskId,
        _listId: req.params.listId
    }).then((removedTaskDoc) =>{
        res.send(removedTaskDoc);
    })
})

app.listen(3000, () =>{
    console.log('Server is listening on port 3000');
})